import json
import os

import jsbeautifier as jsbeautifier

from nowplaying import NowPlaying


CONFIG_PATH = "config.json"


if __name__ == '__main__':
    if not os.path.isfile(CONFIG_PATH):
        print(f"File {CONFIG_PATH} not found, making an empty one, edit it with your app settings")
        with open(CONFIG_PATH, "w") as f:
            config = {
              "sleep": 10,
              "target_dir": "<< put the target path here >>",
              "flask": {
                "redirect_uri": "oauth-callback",
                "port": 8888
              },
              "spotify": {
                "client_id": "<< put your client id here >>",
                "client_secret": "<< put your client secret here >>"
              }
            }
            opts = jsbeautifier.default_options()
            opts.indent_size = 2
            f.write(jsbeautifier.beautify(json.dumps(config), opts))
        exit()
    with open(CONFIG_PATH) as config_file:
        config = json.load(config_file)
        now_playing = NowPlaying(config)
