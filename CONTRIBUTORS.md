# Contributors

The following people have contributed to the development of Now Playing Py. If you submit a pull request, please add your name to the list below.

* 2021-ongoing: Rodrigo Pérez Di Matteo @freakrho
