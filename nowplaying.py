#!/usr/bin/env python3
import json
import os
import shutil
from datetime import datetime, timedelta
import string
import random
from urllib.parse import quote_plus as quote
from base64 import b64encode
from typing import Optional, Dict
from multiprocessing import Process, Manager

import webbrowser
import aiohttp
import asyncio

import flask
import requests

BASE_URL = "https://api.spotify.com"
SCOPE = ["user-read-currently-playing"]
OAUTH_BASE = "https://accounts.spotify.com/authorize/?{parameters}"


class User:
    def __init__(self):
        self.user_id = ""
        self.access_token = ""
        self.expire_time = datetime.now()
        self.refresh_token = ""


class NowPlaying:
    def __init__(self, config: Dict):
        app_config = config["spotify"]
        flask_config = config["flask"]
        self._sleep = config["sleep"]
        self._target_dir = config["target_dir"]
        self._client_id = app_config["client_id"]
        self._client_secret = app_config["client_secret"]
        self._redirect_uri = f"http://localhost:{flask_config['port']}/{flask_config['redirect_uri']}"
        self._token = b64encode(f"{self._client_id}:{self._client_secret}".encode("ascii")).decode("ascii")
        self._authenticated = False
        self._session = aiohttp.ClientSession()
        self._code_verifier = self.randomword(
            random.randrange(43, 128),
            string.ascii_letters + "0123456789_."
        )

        self.user: Optional[User] = None
        self.user = None
        self.manager = Manager()
        self.data = self.manager.dict()
        self.data["authenticated"] = False
        self.data["access_code"] = ""
        self.data["state"] = self.randomword(16)

        self._flask_thread = FlaskThread(self, flask_config)
        self._flask_thread.start()

        try:
            asyncio.get_event_loop().run_until_complete(self.run())
        except KeyboardInterrupt:
            self._flask_thread.terminate()

    @property
    def authenticated(self) -> bool:
        return self.data["authenticated"]

    @property
    def access_code(self) -> str:
        return self.data["access_code"]

    @property
    def state(self) -> str:
        return self.data["state"]

    @staticmethod
    def randomword(length, letters=string.ascii_lowercase):
        return ''.join(random.choice(letters) for _ in range(length))

    @staticmethod
    def get_url(endpoint: str):
        return f"{BASE_URL}/{endpoint}"

    async def get_user(self):
        request = await self._session.post(
            "https://accounts.spotify.com/api/token",
            data={
                "grant_type": "authorization_code",
                "code": self.access_code,
                "redirect_uri": self._redirect_uri,
                # "client_id": self._client_id,
                # "code_verifier": self._code_verifier
            },
            headers={
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": f"Basic {self._token}",
            },
        )
        if request.status == 200:
            data = json.loads(await request.text())
            self.user = User()
            self.user.access_token = data["access_token"]
            self.user.expire_time = datetime.now() + timedelta(seconds=data["expires_in"])
            self.user.refresh_token = data["refresh_token"]

    async def get_current_playing(self):
        request = await self._session.get(
            "https://api.spotify.com/v1/me/player/currently-playing?market=ES",
            params={
            },
            headers={
                "Authorization": f"Bearer {self.user.access_token}",
                "Content-Type": "application/json",
            },
        )
        data = json.loads(await request.text())
        item = data["item"]
        name = item["name"]
        artists = []
        for artist in item["artists"]:
            artists.append(artist["name"])
        self.write_song_title(f"{name} by {', '.join(artists)}")

        biggest_image = None
        width = 0
        for image in item["album"]["images"]:
            if image["width"] > width:
                biggest_image = image
                width = image["width"]
        if biggest_image is not None:
            self.download_image(biggest_image["url"], "cover.jpg")

    async def run(self):
        parameters = {
            "client_id": self._client_id,
            "response_type": "code",
            "redirect_uri": quote(self._redirect_uri),
            "scope": quote(" ".join(SCOPE)),
            "state": self.state,
            # "code_challenge_method": "S256",
            # "code_challenge": hashlib.sha256(self._code_verifier.encode('utf-8')).hexdigest(),
        }
        url = OAUTH_BASE.format(parameters="&".join("{0}={1}".format(*item) for item in parameters.items()))
        webbrowser.open(url)

        while True:
            print(f"Authenticated: {self.authenticated}")
            if self.authenticated:
                if self.user is None:
                    await self.get_user()
                    await self.get_current_playing()
                else:
                    await self.get_current_playing()
                await asyncio.sleep(self._sleep)
            else:
                await asyncio.sleep(0.01)

    def write_song_title(self, text: str):
        f = open(f"{self._target_dir}/song.txt", "w")
        f.write(text)
        f.close()

    def download_image(self, url: str, name: str):
        filename = url.split('/')[-1]
        filepath = f"cache/{filename}"
        if not os.path.isdir("cache"):
            os.mkdir("cache")

        if not os.path.isfile(filepath):
            r = requests.get(url, stream=True)
            if r.status_code == 200:
                # Set decode_content value to True, otherwise the downloaded image file's size will be zero.
                r.raw.decode_content = True

                # Open a local file with wb ( write binary ) permission.
                with open(filepath, 'wb') as f:
                    shutil.copyfileobj(r.raw, f)

        dst_file = f"{self._target_dir}/{name}"
        # HACK: I write the filename into a text file because the 'samefile' method doesn't think they're the same file
        current_file = f"{self._target_dir}/current-{name}.txt"
        if os.path.exists(current_file):
            with open(current_file, "r") as f:
                if f.readline().strip() == filename:
                    return
            os.remove(dst_file)

        shutil.copy(filepath, dst_file)
        with open(current_file, "w") as f:
            f.write(filename)


class FlaskThread:
    def __init__(self, now_playing: NowPlaying, config: Dict):
        self._server = None

        self._now_playing = now_playing
        self._port = config["port"]
        self._redirect_uri = config["redirect_uri"]
        self._app = flask.Flask(__name__)

        self._data = None

        @self._app.route(f"/{self._redirect_uri}")
        def callback():
            if flask.request.args['state'] == self._data["state"]:
                self._data["access_code"] = flask.request.args['code']
                self._data["authenticated"] = True
                return "<html><p>You can close this page</p></html>"
            return "<html><p>Nice try</p></html>"

    def start(self) -> None:
        self._server = Process(target=self.run, args=(self._now_playing.data,))
        self._server.start()

    def run(self, data):
        self._data = data
        self._app.run("localhost", port=self._port, debug=False)

    def terminate(self):
        if self._server is None:
            return
        self._server.terminate()
        self._server.join()
